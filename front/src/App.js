import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import BulkOrderAdmin from './components/Admin/BulkOrderAdmin';
import BulkOrderRequestAdmin from './components/Admin/BulkOrderRequestAdmin';
import ItemsPage from './components/Customer/ItemsPage';


function App() {
  return (
    <Router>
      <Routes>             
        <Route path="/BulkOrderAdmin" element={<BulkOrderAdmin/>} />
        <Route path="/BulkOrderRequestAdmin" element={<BulkOrderRequestAdmin/>} />
        <Route path="/ItemsPage" element={<ItemsPage/>} />
      </Routes>      
    </Router>
  );
}

export default App;
