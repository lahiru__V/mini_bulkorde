import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Typography,
    TextField,
    Button,
    Grid,
    Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper
} from '@material-ui/core';
import axios from 'axios';
import Swal from 'sweetalert2';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    header: {
        backgroundColor: theme.palette.dark,
        color: theme.palette.primary,
        paddingTop: theme.spacing(4),
        textAlign: 'center',
    },
    section: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(10),
        marginLeft: theme.spacing(60),
        marginRight: theme.spacing(60),
    },
    form: {
        marginTop: theme.spacing(3),
    },
    table: {
        marginTop: theme.spacing(3),
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '30px',
    },
}));

function BulkOrderAdmin() {
    const classes = useStyles();
    const [name, setName] = useState('');
    const [type, setType] = useState('');
    const [brand, setBrand] = useState('');
    const [description, setDescription] = useState('');
    const [picture, setPicture] = useState('');
    const uniqueId = generateId();
    const [details, setDetails] = useState([]);
    const [editvalue, setEditvalue] = useState(false);

    useEffect(() => {
        fetchDetails();
    }, []);

    function generateId() {
        let id = '';
        for (let i = 0; i < 9; i++) {
            id += Math.floor(Math.random() * 10);
        }
        return id;
    }

    const fetchDetails = async () => {
        try {
            const response = await axios.get('http://localhost:5000/item/allItem');
            const detailsWithId = response.data.map((details, index) => ({
                id: index + 1,
                ...details
            }));
            setDetails(detailsWithId);
        } catch (error) {
            console.error('Error fetching Item details:', error);
        }
    };

    const handleNameChange = (event) => {
        setName(event.target.value);
    };

    const handleTypeChange = (event) => {
        setType(event.target.value);
    };

    const handleBrandChange = (event) => {
        setBrand(event.target.value);
    };

    const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
    };

    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = { uniqueId, name, type, brand, description, picture };
        console.log(data);
        try {
            await axios.post('http://localhost:5000/item/addItem', data);
            Swal.fire({
                title: "Success!",
                text: "Item Add successfully.",
                icon: 'success',
                confirmButtonText: "OK"
            });
            setTimeout(() => {
                window.location.href = "/BulkOrderAdmin";
            }, 1000);
        } catch (error) {
            console.error(error.message);
            Swal.fire({
                title: "Error!",
                text: "Failed",
                icon: 'error',
                confirmButtonText: "OK"
            });
            setTimeout(() => {
                window.location.href = "/BulkOrderAdmin";
            }, 1000);
        }
    };

    const handleDelete = async (itemId) => {
        try {
            await axios.delete(`http://localhost:5000/item/delete/${itemId}`);
            Swal.fire({
                title: "Success!",
                text: "Item deleted successfully.",
                icon: 'success',
                confirmButtonText: "OK"
            });
            fetchDetails();
        } catch (error) {
            console.error('Error deleting Item:', error);
            Swal.fire({
                title: "Error!",
                text: "Failed to delete Item.",
                icon: 'error',
                confirmButtonText: "OK"
            });
        }
    };

    const handleEdit = (uniqueId, name, type, brand, description, picture) => {
        setName(name);
        setType(type);
        setBrand(brand);
        setDescription(description);
        setPicture(picture);
        localStorage.setItem("uniqueId", uniqueId);
        setEditvalue(true);
    };

    const handleEditSubmit = async (e) => {
        e.preventDefault();
        const uniqueId = localStorage.getItem("uniqueId");
        const data = { uniqueId, name, type, brand, description, picture };
        try {
            await axios.put(`http://localhost:5000/item/update`, data);
            Swal.fire({
                title: "Success!",
                text: "Item updated successfully.",
                icon: 'success',
                confirmButtonText: "OK"
            });
            fetchDetails();
            setEditvalue(false);
            setTimeout(() => {
                window.location.href = "/BulkOrderAdmin";
            }, 1000);
        } catch (error) {
            console.error('Error updating Item:', error);
            Swal.fire({
                title: "Error!",
                text: "Failed to update Item.",
                icon: 'error',
                confirmButtonText: "OK"
            });
            setTimeout(() => {
                window.location.href = "/BulkOrderAdmin";
            }, 1000);
        }
    };

    return (
        <div className={classes.root}>
            {/* <Navbar /> */}
            <div className={classes.header}>
                {editvalue ? (
                    <Typography variant="h3" component="h1">Edit Instrument</Typography>
                ) : (
                    <Typography variant="h3" component="h1">Add New Instrument</Typography>
                )}
            </div>
            <hr style={{ width: 100 }}></hr>
            <section className={classes.section}>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <Grid container spacing={3} direction="column">
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                id="name"
                                label="Instrument Name"
                                value={name}
                                onChange={handleNameChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                id="type"
                                label="Instrument Type"
                                value={type}
                                onChange={handleTypeChange}
                                type='type'
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="brand"
                                label="Brand"
                                value={brand}
                                onChange={handleBrandChange}
                                required
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                id="picture"
                                label="Image URL"
                                value={picture}
                                onChange={handlePictureChange}
                                required
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                id="description"
                                label="Description"
                                multiline
                                rows={4}
                                value={description}
                                onChange={handleDescriptionChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            {editvalue ? (
                                <Button variant="contained" color="primary" onClick={handleEditSubmit}>
                                    Edit
                                </Button>
                            ) : (
                                <Button variant="contained" color="primary" type="submit">
                                    Save
                                </Button>
                            )}
                        </Grid>
                    </Grid>
                </form>
            </section>
            <section className={classes.table}>
                <TableContainer component={Paper}>
                    <Table aria-label="table">
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell>Type</TableCell>
                                <TableCell>Brand</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell>Picture</TableCell>
                                <TableCell>Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {details.map((details) => (
                                <TableRow key={details.id}>
                                    <TableCell>{details.uniqueId}</TableCell>
                                    <TableCell>{details.name}</TableCell>
                                    <TableCell>{details.type}</TableCell>
                                    <TableCell>{details.brand}</TableCell>
                                    <TableCell>{details.description}</TableCell>
                                    <TableCell>
                                        <img src={details.picture} alt="Item" style={{ width: '50px', height: '50px' }} />
                                    </TableCell>
                                    <TableCell>
                                        <Button variant="contained" color="secondary" onClick={() => handleDelete(details.uniqueId)}>
                                            Delete
                                        </Button>{' '}
                                        <Button variant="contained" color="primary" onClick={() => handleEdit(details.uniqueId, details.name, details.type, details.brand, details.description, details.picture)}>
                                            Edit
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </section>
            {/* <Footer /> */}
        </div>
    );
}

export default BulkOrderAdmin;
