import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardContent,
    Grid,
    Typography,
    TextField,
    Button,
    Divider,
    Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper
} from '@material-ui/core';
import axios from 'axios';
import Swal from 'sweetalert2';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    section: {
        marginTop: theme.spacing(5),
        paddingLeft: theme.spacing(5),
        paddingRight: theme.spacing(5),
        marginBottom: theme.spacing(10),
    },
    card: {
        maxWidth: '100%',
    },
    cardContent: {
        flexGrow: 1,
    },
    button: {
        marginTop: theme.spacing(2),
    },
    quantityField: {
        marginTop: theme.spacing(2),
    },
    header: {
        textAlign: 'center',
        marginBottom: theme.spacing(3),
    },
    table: {
        marginTop: theme.spacing(3),
        paddingLeft: '100px',
        paddingRight: '100px',
        paddingBottom: '30px',
    },
}));

function ItemsPage() {
    const classes = useStyles();
    const [details, setDetails] = useState([]);
    const [orderdetails, setOrderDetails] = useState([]);


    useEffect(() => {
        fetchDetails();
        fetchOrderDetails();
    }, []);

    const fetchOrderDetails = async () => {
        try {
            const response = await axios.get('http://localhost:5000/request/allRequests');
            const detailsWithId = response.data.map((details, index) => ({
                id: index + 1,
                ...details
            }));
            setOrderDetails(detailsWithId);
        } catch (error) {
            console.error('Error fetching Item details:', error);
        }
    };

    const fetchDetails = async () => {
        try {
            const response = await axios.get('http://localhost:5000/item/allItem');
            const detailsWithId = response.data.map((details, index) => ({
                id: index + 1,
                ...details
            }));
            setDetails(detailsWithId);
        } catch (error) {
            console.error('Error fetching Item details:', error);
        }
    };

    const handleApply = async (product) => {
        const quantityInput = document.getElementById(`quantity-${product.id}`).value;
        const quantity = parseInt(quantityInput);

        if (isNaN(quantity) || quantity <= 0) {
            Swal.fire({
                title: 'Invalid Quantity',
                text: 'Please enter a valid quantity greater than 0.',
                icon: 'error',
                confirmButtonText: 'OK'
            });
            return;
        }

        const data = {
            rid: Math.floor(Math.random() * 1000),
            itemId: product.id,
            item: product.name,
            quantity: quantity,
            status: 'Pending',
            price: '0',
            mail: 'b.a.amadhihansani02@gmail.com' 
        };

        try {
            await axios.post('http://localhost:5000/request/addRequest', data);
            Swal.fire({
                title: 'Product Requested',
                text: product.name + ' Requested successfully!',
                icon: 'success',
                confirmButtonText: 'OK'
            });
            setTimeout(() => {
                window.location.href = "/ItemsPage";
            }, 1000);
        } catch (error) {
            console.error('Error requesting item:', error);
            Swal.fire({
                title: 'Error',
                text: 'An error occurred while requesting the item. Please try again later.',
                icon: 'error',
                confirmButtonText: 'OK'
            });
            setTimeout(() => {
                window.location.href = "/ItemsPage";
            }, 1000);
        }
    };

    const handleDelete = async (rid) => {
        try {
            await axios.delete(`http://localhost:5000/request/deleteRequest/${rid}`);
            Swal.fire({
                title: "Success!",
                text: "Request deleted successfully.",
                icon: 'success',
                confirmButtonText: "OK"
            });
            fetchOrderDetails();
        } catch (error) {
            console.error('Error deleting Item:', error);
            Swal.fire({
                title: "Error!",
                text: "Failed to delete Item.",
                icon: 'error',
                confirmButtonText: "OK"
            });
        }
    };

    const handleAccept = async (rid, itemId, item, quantity, status, price, mail) => {
        var status = 'Completed';
        const data = { rid, itemId, item, quantity, status, price, mail };
        try {
            await axios.put('http://localhost:5000/request/updateReq', data);
        } catch (error) {
            console.error(error.message);
        }
    };


    return (
        <div className={classes.root}>
            <br />
            <Typography variant="h4" className={classes.header}>
                Bulk Order Request
            </Typography>
            <Grid container spacing={3} className={classes.section}>
                {details.map(product => (
                    <Grid item xs={12} sm={6} md={4} key={product.uniqueId}>
                        <Card className={classes.card}>
                            <CardContent className={classes.cardContent}>
                                <Typography variant="h5" gutterBottom>{product.name}</Typography>
                                <Typography variant="subtitle1" gutterBottom>{product.type}</Typography>
                                <Divider style={{ marginBottom: '10px' }} />
                                <Typography variant="body2" gutterBottom><strong>Brand:</strong> {product.brand}</Typography>
                                <Typography variant="body2" gutterBottom><strong>Description:</strong></Typography>
                                <Typography variant="body2" paragraph>{product.description}</Typography>
                                <Divider style={{ marginBottom: '10px' }} />
                                <img src={product.picture} alt={product.name} style={{ width: '250px', height: '250px', marginBottom: '10px' }} />
                                <TextField
                                    type="number"
                                    label="Quantity"
                                    defaultValue={product.quantity}
                                    InputProps={{ inputProps: { min: 1 } }}
                                    fullWidth
                                    id={`quantity-${product.id}`}
                                    className={classes.quantityField}
                                />
                                <Button variant="contained" color="primary" className={classes.button} onClick={() => handleApply(product)}>Order</Button>
                            </CardContent>
                        </Card>
                    </Grid>
                ))}
            </Grid>
            <div className={classes.header}>
                <Typography variant="h4" component="h1">Manage Your Requests</Typography>
            </div>
            <hr style={{ width: 100 }} />
            <section className={classes.table}>
                <TableContainer component={Paper}>
                    <Table aria-label="ticket table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Request ID</TableCell>
                                <TableCell>Item ID</TableCell>
                                <TableCell>Item Name</TableCell>
                                <TableCell>Quantity</TableCell>
                                <TableCell>Status</TableCell>
                                <TableCell>Price</TableCell>
                                <TableCell>Mail</TableCell>
                                <TableCell>Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {orderdetails.map((details) => (
                                <TableRow key={details.id}>
                                    <TableCell>{details.rid}</TableCell>
                                    <TableCell>{details.itemId}</TableCell>
                                    <TableCell>{details.item}</TableCell>
                                    <TableCell>{details.quantity}</TableCell>
                                    <TableCell style={{ fontWeight: 'bold' }}>{details.status}</TableCell>
                                    <TableCell>{details.price}</TableCell>
                                    <TableCell>{details.mail}</TableCell>
                                    <TableCell>
                                        {details.status === 'Accepted' ? (
                                            <>
                                                <Button variant="contained" color="success" onClick={() => handleAccept(details.rid, details.itemId, details.item, details.quantity, details.status, details.price, details.mail)}>
                                                    Accept
                                                </Button>
                                                &nbsp;  &nbsp;
                                                <Button variant="contained" color="secondary" onClick={() => handleDelete(details.rid)}>
                                                    Reject
                                                </Button>
                                            </>
                                        ) : (
                                            <Button variant="contained" color="secondary" disabled>
                                                Disabled
                                            </Button>
                                        )}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </section>
        </div>
    );
}

export default ItemsPage;
